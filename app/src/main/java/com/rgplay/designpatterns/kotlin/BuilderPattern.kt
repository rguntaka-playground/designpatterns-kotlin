package com.rgplay.designpatterns.kotlin

class BuilderPattern(
    val cheese: Boolean,
    val chicken: Boolean,
    val onions: Boolean
) {
    class Builder {
        private var cheese: Boolean = true
        private var chicken: Boolean = true
        private var onions: Boolean = false

        fun cheese(wantIt: Boolean) = apply { cheese = wantIt }
        fun chicken(wantIt: Boolean) = apply { chicken = wantIt }
        fun onions(wantIt: Boolean) = apply { onions = wantIt }

        fun build() = BuilderPattern(cheese, chicken, onions)
    }
}