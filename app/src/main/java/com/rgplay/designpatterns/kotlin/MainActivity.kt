package com.rgplay.designpatterns.kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // invoking Builder
        val hamburger = BuilderPattern.Builder().cheese(true).chicken(false).onions(true).build()

        // Android has many default builder pattern classes. Example:
        // MaterialAlertDialogBuilder(this).setIcon(R.drawable.ic_launcher_background).setBackgroundInsetBottom(R.color.black)
    }
}