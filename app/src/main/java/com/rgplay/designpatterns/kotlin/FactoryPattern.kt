package com.rgplay.designpatterns.kotlin

//ViewModel use Factory Pattern

enum class DialogType{
    DIALOG_CREATE_CHAT,
    DIALOG_DELETE_MESSAGE,
    DIALOG_EDIT_MESSAGE
}

// sealed class is type of class with restricted hierarchy. when we know that a variant is one of the known types, then, sealed class is a optimal alternative
// It is used when it is known in advance that a type will conform to one of the subclass types.
// Sealed classes ensure type-safety by restricting the types to be matched at compile-time rather than at runtime

sealed class Dialog{
    object CreateChatDialog: Dialog()
    object DeleteMessageDialog: Dialog()
    object EditMessageDialog: Dialog()
}

/*
 Factory pattern used to decide the class/ object to be used based on the input type
 */
object DialogFactory{
    fun createDialog(dialogType: DialogType): Dialog{
        return when(dialogType){
            DialogType.DIALOG_CREATE_CHAT -> Dialog.CreateChatDialog
            DialogType.DIALOG_DELETE_MESSAGE -> Dialog.DeleteMessageDialog
            DialogType.DIALOG_EDIT_MESSAGE -> Dialog.EditMessageDialog
        }
    }
}